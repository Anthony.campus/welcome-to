let createCard = () => {
    let card = createElement("div", "card")
    let flip_card = createElement("div", "flip-card ")
    let flip_card_inner = createElement("div", "flip-card-inner")
    let flip_card_front = createElement("div", "flip-card-front bg-linear-color")
    let flip_card_back = createElement("div", "flip-card-back bg-linear-color")
    
    let lore = createElement("lore", "")
    lore.id = "MyNumber"
    lore.innerHTML = getNumber() || "OUPS"

    let img = createElement("img", "")
    img.id = "MyImg"
    img.style = "width:300px;height:300px;"
    img.src = getImg() || "http://www.jlfavero.fr/wp-content/uploads/2019/01/carte-danniversaire-en-chti-a-imprimer-elegant-la-barraque-frittes-of-carte-danniversaire-en-chti-a-imprimer.jpg"

    card.appendChild(flip_card)
    flip_card.appendChild(flip_card_inner)
    flip_card_inner.appendChild(flip_card_front)
    flip_card_inner.appendChild(flip_card_back)
    
    flip_card_front.appendChild(img)
    flip_card_back.appendChild(lore)

    return card
  }

let createElement = (type, classList) => {
    let element = document.createElement(type);
    element.classList = classList
    return element
}

let list = [
    './img/piscine.png', './img/piscine.png', './img/piscine.png', './img/piscine.png', './img/piscine.png', './img/piscine.png', './img/piscine.png', './img/piscine.png', './img/piscine.png',
    './img/agence_interim.png', './img/agence_interim.png', './img/agence_interim.png', './img/agence_interim.png', './img/agence_interim.png', './img/agence_interim.png', './img/agence_interim.png', './img/agence_interim.png', './img/agence_interim.png',
    './img/numero_bis.png', './img/numero_bis.png', './img/numero_bis.png', './img/numero_bis.png','./img/numero_bis.png', './img/numero_bis.png', './img/numero_bis.png', './img/numero_bis.png', './img/numero_bis.png',
    './img/paysagiste.png', './img/paysagiste.png', './img/paysagiste.png', './img/paysagiste.png', './img/paysagiste.png', './img/paysagiste.png', './img/paysagiste.png', './img/paysagiste.png', './img/paysagiste.png', './img/paysagiste.png', './img/paysagiste.png', './img/paysagiste.png', './img/paysagiste.png', './img/paysagiste.png', './img/paysagiste.png', './img/paysagiste.png', './img/paysagiste.png', './img/paysagiste.png', 
    './img/agent_immobilier.png', './img/agent_immobilier.png', './img/agent_immobilier.png', './img/agent_immobilier.png', './img/agent_immobilier.png', './img/agent_immobilier.png', './img/agent_immobilier.png', './img/agent_immobilier.png', './img/agent_immobilier.png', './img/agent_immobilier.png', './img/agent_immobilier.png', './img/agent_immobilier.png', './img/agent_immobilier.png', './img/agent_immobilier.png', './img/agent_immobilier.png', './img/agent_immobilier.png', './img/agent_immobilier.png', './img/agent_immobilier.png', 
    './img/geometre.png', './img/geometre.png', './img/geometre.png', './img/geometre.png', './img/geometre.png', './img/geometre.png', './img/geometre.png', './img/geometre.png', './img/geometre.png', './img/geometre.png', './img/geometre.png', './img/geometre.png', './img/geometre.png', './img/geometre.png', './img/geometre.png', './img/geometre.png', './img/geometre.png', '.img/geometre.png']

function getRandom(min, max) {
    return Math.round(Math.random() * (max - min) + min )
}

function getImg(){
    let rand = getRandom(0, list.length - 1)
    let result = list[rand]
    list.splice(rand, 1)
    return result
}

let pacquet = [1,1,1,2,2,2,14,14,14,15,15,15,3,3,3,3,13,13,13,13,4,4,4,4,4,12,12,12,12,12,5,5,5,5,5,5,11,11,11,11,11,11,6,6,6,6,6,6,6,10,10,10,10,10,10,10,7,7,7,7,7,7,7,7,9,9,9,9,9,9,9,9,8,8,8,8,8,8,8,8,8]

function getNumber() {
    let rand = getRandom(0, pacquet.length - 1)
    let result = pacquet[rand]
    pacquet.splice(rand, 1)
    return result
}
